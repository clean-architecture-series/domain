# domain

## vitepress setup
 
nvm use v23.3.0
npm add -D vitepress
npx vitepress init

## run

npm run docs:dev

Build and Test Locally
Run this command to build the docs:

## test locally

$ npm run docs:build
Once built, preview it locally by running:

sh
$ npm run docs:preview
 