# Functional domain modelling

![Insanity is doing the same thing over and over again and expecting different results](/images/insanity.png)


## Source code

Source code for the Customer Management Domain is available on GitLab [here](https://gitlab.com/clean-architecture-series/domain)

```
git clone https://gitlab.com/clean-architecture-series/domain.git
```




## Preamble

We will use functional programming and DDD techniques
to implement business domain.

The Domain layer uses Java 21 and the VAVR library (optionally, it also uses Lombok).
It does not have any dependencies on other application layers.


## Inspired by
 
1. Clean Architecture by Robert C. Martin
2. Domain Driven Design by Eric Evans.
3. Functional Programming. [VAVR](https://docs.vavr.io) by Daniel Dietrich
4. [From object oriented to functional domain modeling](https://youtu.be/K6BmGBzIqW0) by Mario Fusco 
5. [Domain Modeling Made Functional](https://youtu.be/2JB1_e5wZmU) by Scott Wlaschin   
 <small>for his other talks and articles see his website: [F# for Fun and Profit](https://fsharpforfunandprofit.com/)</small>
 


## We will learn

1. how to make illegal states unrepresentable by using **Applicatives**.
2. avoid nulls, exceptions and defensive programing by using **Monads**.
3. avoid side effects, by using **pure functions**.
4. use **immutable state**  

