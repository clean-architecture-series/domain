import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base: '/domain/',
  outDir: '../public',
  title: "Domain",
  description: "Functional domain modelling",
  head: [
    ['link', { rel: 'icon', type: 'image/svg+xml', href: '/domain/favicon.svg' }],
  ],
  markdown: {
    lineNumbers: true,
    theme: 'light-plus' // Change this to your desired theme
  },
  themeConfig: {
    lastUpdated: false,
    externalLinkIcon: true,
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'JK blog', link: 'https://jkazys.gitlab.io' }
    ],

    sidebar: [
      {
        base: '/',
        items: [
          { text: 'Business domain', link: 'domain'},
          { text: 'Customer', link: 'customer', base: '/customer/'},
          { text: 'Active Customer', link: 'active', base: '/customer/',
          items: [
            { text: 'National Id', link: 'nationalId' },
            { text: 'Name', link: 'name' },
            { text: 'Middle name', link: 'middleName' },
            { text: 'Family name', link: 'familyName' },
            { text: 'Contact Info', link: 'contacts',
            items: [
                        { text: 'Email', link: 'email' },
                        { text: 'Mobile', link: 'mobile' },
                   ]
            },
          ]
         },
         { text: 'Unsubscribed Customer', link: 'unsubscribed', base: '/customer/',

          items: [
            { text: 'National Id', link: 'nationalId' },
            { text: 'Termination reason', link: 'termination' },
          ]
          },

          { text: 'Testing', link: 'intro', base: '/testing/'},
          { text: 'What next', link: 'end'},
         
        ]
      }
    ],
    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/clean-architecture-series/domain' }
    ],
    
    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2024-present Kazys Jocionis'
    }
  }
})
