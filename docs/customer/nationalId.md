# National Id

<<< ../../src/main/java/domain/customer/vo/NationalId.java#1

<!--@include: abstract-vo-link.md-->

::: details Validation constraints
<!--@include: rules/id.md-->
:::
::: details VO Instantiation {open}
<!--@include: instantiation.md-->
:::


## Validation


if all is valid, it returns NationalId 

If it is not valid, it returns the collected validation errors, for instance:

```console
"Id: is mandatory",
"Id: Must start with one of: 1,2,3,4,5,6.",
"Id: must have exactly 11 digits",
```

if it is valid its unique constraints are further validated in application layer

If id is taken by another ActiveCustomer or UnsubscribedCustomer it will return corresponding error as well. 

