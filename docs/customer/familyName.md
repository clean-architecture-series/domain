# Family Name

<<< ../../src/main/java/domain/customer/vo/FamilyName.java#1

<!--@include: abstract-vo-link.md-->

::: details Validation constraints
<!--@include: rules/familyName.md-->
:::

::: details VO Instantiation
<!--@include: instantiation.md-->
:::


## Validation

if all is valid, it returns FamilyName

If it is not valid, it returns the collected validation errors, for instance:

```console
"Family name: is mandatory",
"Family name: length must be 1 to 30",
"Family name: must contain letters only"
```
