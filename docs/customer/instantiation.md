
All our VO are **immutable** have **private constructors**
and are instantiated using **Static Factory Method**
For validation we are using **Applicative Functors**.
In essence if validation fails it returns sequence of collected validation errors. 
It calls constructor only if thero are no validation errors at all.

This is part of **Make illegal states unrepresentable**  

> TODO link to validation using **Applicative Functors**
 
