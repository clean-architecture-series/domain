# Unsubscribed Customer

<<< ../../src/main/java/domain/customer/UnsubscribedCustomer.java#1
<!--@include: abstract-entity-link.md-->

As defined in the business rules, the implementation has only `NationalId` and `Option<TerminationReason>`, with no behaviour at all.

::: details rules

<!--@include: rules/unsubscribed.md-->

  

:::
