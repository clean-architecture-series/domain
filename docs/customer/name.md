# First Name

<<< ../../src/main/java/domain/customer/vo/Name.java#1

<!--@include: abstract-vo-link.md-->

::: details Validation constraints
<!--@include: rules/name.md-->
:::

::: details VO Instantiation
<!--@include: instantiation.md-->
:::

## Validation

if all is valid, it returns Name

If it is not valid, it returns the collected validation errors, for instance:

```console
"Name: is mandatory",
"Name: length must be 1 to 20 letters",
"Name: must contain letters only",
```
