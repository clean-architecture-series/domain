# Customer

In DDD, a **Domain Entity** is compared by identity; in the case of `Customer`, it's by `NationalId`.

We cannot use the Java `record` to implement entities because their `equals` and `hashCode` are based on all the fields.


## Lifecycle

We use sealed classes (ADT sum types) to represent the two possible lifecycle stages of a `Customer`: 
`ActiveCustomer` and `UnsubscribedCustomer`. 



<<< ../../src/main/java/domain/customer/Customer.java#1

As we will see later, `ActiveCustomer` and `UnsubscribedCustomer` implementations use the immutable state pattern.

They have different fields and behaviors. Unnecessary fields and behaviors will not be present.


## Exhaustiveness

Java 21 finalized an exhaustive switch for sealed types. This means that all possible values must be covered at compile time.

This prevents runtime errors and facilitates code refactoring as the domain evolves.

<!-- done -->
