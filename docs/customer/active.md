# Active Customer

<<< ../../src/main/java/domain/customer/ActiveCustomer.java#1


### State
As promoted in DDD, we **make illegal states unrepresentable**

Our Domain Entities are immutable and constructed entirely using Value Objects. A Domain Entity is and always stays in a valid state, which makes reasoning easier.

In turn, Value Objects are validated upon construction and are immutable as well. They prevent what is called **Primitive Obsession** and bring a series of other benefits.

We make explicitly clear which fields are mandatory and which are optional. Optionality is represented using the `Option<T>` **Monad**.

<<< ../../src/main/java/domain/customer/ActiveCustomer.java#4
This particular Monad is useful to prevent null pointer exceptions or defensive programming scattered all over the codebase.

It forces handling the absence of a value in a functional/declarative way.


> TODO link to Category Theory and Monads
> 
## Behaviour

### Can be updated.

It transitions into another valid **ActiveCustomer** immutable state.

<<< ../../src/main/java/domain/customer/ActiveCustomer.java#2

### Can be unsubscribed.
It transitions into the **UnsubscribedCustomer** state, which is another immutable Domain Entity 
representing a different state.

As we can see, we use the state pattern.

Different states can have different fields and behaviours. Unnecessary fields and behaviours should not be present in a Domain Entity.

<<< ../../src/main/java/domain/customer/ActiveCustomer.java#3

<!-- done -->