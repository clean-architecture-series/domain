# Termination

<<< ../../src/main/java/domain/customer/vo/TerminationReason.java#1

<!--@include: abstract-vo-link.md-->

::: details Validation constraints
<!--@include: rules/termination.md-->
:::

::: details VO Instantiation
<!--@include: instantiation.md-->
:::


## Validation of optional TerminationReason

**When the value is not present:**

It always returns a valid result: `None<TerminationReason>`.

**When the value is present:**

If validation succeeds, it returns `Some<TerminationReason>`.

Otherwise, it returns the collected validation errors. For example:


```console
"Termination reason: length must be 10 to 1024"
```