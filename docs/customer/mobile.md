# Mobile

<<< ../../src/main/java/domain/customer/vo/Mobile.java#1

<!--@include: abstract-vo-link.md-->

## Validation constraints
::: details Validation constraints{open}
- mandatory
- must have exactly 9 digits
- unique (validated at application layer)
:::

::: details VO Instantiation
<!--@include: instantiation.md-->
:::

## Validation if Mobile is present in contacts list 

if value is valid, it returns Mobile

If it is not valid, it returns the collected validation errors, for instance:

```console
"Mobile: is mandatory",
"Mobile: must have exactly 9 digits",
```
