# Email

<<< ../../src/main/java/domain/customer/vo/Email.java#1

<!--@include: abstract-vo-link.md-->

## Validation constraints
::: details Validation constraints{open}
- mandatory  
- must be valid email
- unique (validated at application layer)
:::

::: details VO Instantiation
<!--@include: instantiation.md-->
:::

## Validation if Email is present in contacts list 

if value is valid, it returns Email

If it is not valid, it returns the collected validation errors, for instance:

```console
"Email: is mandatory"
"Email: is not a valid address"
```
