# Middle Name

<<< ../../src/main/java/domain/customer/vo/MiddleName.java#1

<!--@include: abstract-vo-link.md-->

::: details Validation constraints
<!--@include: rules/middleName.md-->
:::

::: details VO Instantiation
<!--@include: instantiation.md-->
:::


## Validation of optional MiddleName

**When the value is not present:**

It always returns a valid result: `None<MiddleName>`.

**When the value is present:**

If validation succeeds, it returns `Some<MiddleName>`.

Otherwise, it returns the collected validation errors. For example:


```console
"Middle name: length must be 1 to 20 letters",
"Middle name: must contain letters only",
```