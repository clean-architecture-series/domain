# Customer Management Domain

As an example, we will implement a Customer Management Domain following DDD principles.

> **NOTE**. For demonstration purposes, we will use somewhat simplified domain rules, as our main focus is on the technical solution.

Our domain experts expressed the following business rules:

## Lifecycle

Customer has a lifecycle with two stages:

1. Active Customer
2. Unsubscribed customer

## Business rules

### Active customer

#### State

1. National Id
<!--@include: customer/rules/id.md-->

2. First Name
<!--@include: customer/rules/name.md--> 
   
3. Middle Name
<!--@include: customer/rules/middleName.md-->

4. Family Name
<!--@include: customer/rules/familyName.md-->


4. A set of Contact Information entries
<!--@include: customer/rules/contacts.md-->

#### Behaviour

1. Active Customer must be able to update name, middle name, family name and contact information.
2. Active Customer has to be allowed to unsubscribe at free will.

### Unsubscribed customer.
<!--@include: customer/rules/unsubscribed.md-->
