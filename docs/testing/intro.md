# Testing

**"Program testing can be used to show the presence of bugs, but never to show their absence!"**  
Edsger Wybe Dijkstra in his "Notes On Structured Programming" 1970

## Unit testing

We cannot definitively prove the absence of bugs or the correctness of the program. 
Typically, the number of permutations is too vast to make exhaustive testing realistic or feasible. 
However, this doesn't negate the importance of testing. 
In fact, in the domain layer, high test coverage is essential.

![coverage](/images/coverage.png)
