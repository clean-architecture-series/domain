package domain.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustBeAsciiPrintable;

import static org.assertj.core.api.Assertions.assertThat;


class TestMustBeAsciiPrintable {


    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"ABC\n", "a\tb"})
    void testInvalid(String input) {

        var validation = MustBeAsciiPrintable.validate(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("must contain only letters, digits or spaces");

    }

    @ParameterizedTest
    @ValueSource(strings = {" ", "jonas@jonaitus", "Abc 123 !"})
    void testValid(String input) {

        var validation = MustBeAsciiPrintable.validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);
    }

}
