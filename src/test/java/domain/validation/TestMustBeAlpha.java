package domain.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustBeAlpha;

import static org.assertj.core.api.Assertions.assertThat;


class TestMustBeAlpha {


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "123", "A Z"})
    void testInvalid(String input) {

        var validation = MustBeAlpha.validate(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("must contain letters only");

    }

    @ParameterizedTest
    @ValueSource(strings = {"Abc"})
    void testValid(String input) {

        var validation = MustBeAlpha.validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);
    }

}
