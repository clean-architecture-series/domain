package domain.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustHaveLenghtInRange;

import static org.assertj.core.api.Assertions.assertThat;


class TestLengthRange {


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "1", "1234"})
    void testInvalid(String input) {

        var validation = MustHaveLenghtInRange.of(2, 3).validate(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("length must be 2 to 3");

    }

    @ParameterizedTest
    @ValueSource(strings = {"12", "ABC"})
    void testValid(String input) {

        var validation = MustHaveLenghtInRange.of(2, 3).validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);
    }

}
