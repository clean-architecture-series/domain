package domain.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustBeNotNull;

import static org.assertj.core.api.Assertions.assertThat;


class TestMustBeNotNull {


    @ParameterizedTest
    @NullAndEmptySource
    void testNull(String input) {

        var validation = MustBeNotNull.validate(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("is mandatory");

    }


    @ParameterizedTest
    @ValueSource(strings = {" ", "ABC"})
    void testNotNull(String input) {

        var validation = MustBeNotNull.validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);


    }

}
