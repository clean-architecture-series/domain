package domain.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustBeEmail;

import static org.assertj.core.api.Assertions.assertThat;


class TestMustBeEmail {


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "123", "AZ"})
    void testInvalid(String input) {

        var validation = MustBeEmail.validate(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("is not a valid address");

    }

    @ParameterizedTest
    @ValueSource(strings = {"jonas@jonaitis.com"})
    void testValid(String input) {

        var validation = MustBeEmail.validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);
    }

}
