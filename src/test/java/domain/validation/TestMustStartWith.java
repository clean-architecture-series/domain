package domain.validation;

import io.vavr.collection.LinkedHashSet;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustStartWithOneOf;

import static org.assertj.core.api.Assertions.assertThat;


class TestMustStartWith {


    @ParameterizedTest
    @NullSource
    @ValueSource(longs = {0l, 7L, 8L, 9L})
    void testInvalid(Long input) {

        var validation = new MustStartWithOneOf<>(
                LinkedHashSet.of(1L, 2L, 3L, 4L, 5L, 6L)
        ).validate(input);

        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("must start with one of: 1,2,3,4,5,6.");

    }

    @ParameterizedTest
    @ValueSource(longs = {666666})
    void testValid(Long input) {

        var validation = new MustStartWithOneOf<>(
                LinkedHashSet.of(1L, 2L, 3L, 4L, 5L, 6L)
        ).validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);
    }

}
