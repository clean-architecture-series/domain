package domain.validation;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import validation.MustHaveExactLength;

import static org.assertj.core.api.Assertions.assertThat;


class TestLengthExact {


    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "1", "12345", "123456789"})
    void testInvalid(String input) {

        var validation = MustHaveExactLength.of(6).validate(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).isEqualTo("length must be exactly 6");

    }

    @ParameterizedTest
    @ValueSource(strings = {"123456"})
    void testValid(String input) {

        var validation = MustHaveExactLength.of(6).validate(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(input);
    }

}
