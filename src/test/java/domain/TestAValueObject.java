package domain;

import domain.customer.vo.FamilyName;
import domain.customer.vo.Name;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TestAValueObject {

    @Test
    void testEquals() {
        var result = Name.of("Jonas").getOrNull();
        var mustBeEqual = Name.of("Jonas").getOrNull();
        var mustNotBeEqual = Name.of("Petras").getOrNull();
        Name nullName = null;
        var differentType = FamilyName.of("Jonas").getOrNull();


        assertThat(mustBeEqual).isNotNull().isExactlyInstanceOf(Name.class);
        assertThat(mustNotBeEqual).isNotNull().isExactlyInstanceOf(Name.class);
        assertThat(nullName).isNull();
        assertThat(differentType).isNotNull().isExactlyInstanceOf(FamilyName.class);

        assertThat(result)
                .isNotNull()
                .isExactlyInstanceOf(Name.class)
                .isEqualTo(result)
                .isEqualTo(mustBeEqual)
                .isNotEqualTo(null)
                .isNotEqualTo(mustNotBeEqual)
                .isNotEqualTo(nullName)
                .isNotEqualTo(differentType);


    }


    @Test
    void testToString() {

        var result = Name.of("Jonas").map(Name::toString).getOrNull();
        assertThat(result).isEqualTo("Name(Jonas)");

    }
}