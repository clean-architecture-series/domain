package domain;

import domain.customer.ActiveCustomer;
import domain.customer.UnsubscribedCustomer;
import domain.customer.vo.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;
import validation.Flattener;

import static org.assertj.core.api.Assertions.assertThat;

class TestCustomerEntity {

    @Test
    void testEquals() {

        var jonas = activeCustomer(10000000000L, "Jonas");
        var john = activeCustomer(10000000000L, "John");
        var unsubscribedJonas = unsubscribedCustomer(10000000000L, "Jonas");
        var petras = activeCustomer(60000000000L, "Petras");
        var withNullId = customerWithNullId();


        assertThat(jonas).isEqualTo(john).hasSameHashCodeAs(john)
                .isEqualTo(unsubscribedJonas).hasSameHashCodeAs(unsubscribedJonas)
                .isNotEqualTo(petras).doesNotHaveSameHashCodeAs(petras);

        assertThat(withNullId).isNotEqualTo(jonas)
                .doesNotHaveSameHashCodeAs(jonas);


    }


    private Validation<Seq<String>, ActiveCustomer> activeCustomer(Long id, String name) {

        var contacts = HashSet.of(Mobile.of(800000001), Email.of("jonas@jonaitis.com"));
        Validation<Seq<String>, Set<Contact>> contactsSet = Validation.sequence(contacts).map(HashSet::ofAll);


        var customer = NationalId.of(id)
                .combine(Name.of(name))
                .combine(MiddleName.of(null))
                .combine(FamilyName.of("Jonaitis"))
                .combine(contactsSet)
                .ap(ActiveCustomer::new)
                .mapError(Flattener::flatten);

        assertThat(customer.isValid()).isTrue();
        assertThat(customer.getOrNull()).isExactlyInstanceOf(ActiveCustomer.class);

        return customer;

    }

    private Validation<Seq<String>, UnsubscribedCustomer> unsubscribedCustomer(Long id, String name) {
        var customer = activeCustomer(id, name).combine(TerminationReason.of("I am not using it.")).ap(ActiveCustomer::unsubscribe).mapError(Flattener::flatten);

        assertThat(customer.isValid()).isTrue();
        assertThat(customer.getOrNull()).isExactlyInstanceOf(UnsubscribedCustomer.class);
        return customer;

    }

    private ActiveCustomer customerWithNullId() {
        return new ActiveCustomer(null, null, null, null, null);


    }

}