package domain.customer;

import domain.customer.vo.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.control.Validation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import validation.Flattener;

import static org.assertj.core.api.Assertions.assertThat;

class UnsubscribeActiveCustomerTest {

    private Validation<Seq<String>, ActiveCustomer> activeCustomer;


    @Test
    void testSuccess() {


        var result = activeCustomer
                .combine(TerminationReason.of("I am not using it."))
                .ap(ActiveCustomer::unsubscribe)
                .mapError(Flattener::flatten);

        assertThat(result.isValid()).isTrue();
        assertThat(result.getOrNull()).isExactlyInstanceOf(UnsubscribedCustomer.class);


    }

    @Test
    void testFail() {

        var result = activeCustomer
                .combine(TerminationReason.of("I am not \t using it.\n"))
                .ap(ActiveCustomer::unsubscribe)
                .mapError(Flattener::flatten);

        assertThat(result.isValid()).isFalse();
        assertThat(result.getError()).isInstanceOf(List.class);
        assertThat(result.getError()).hasSize(1);


    }

    @BeforeEach
    void init() {

        var contacts = HashSet.of(Mobile.of(800000001), Email.of("jonas@jonaitis.com"));
        Validation<Seq<String>, Set<Contact>> contactsSet =
                Validation.sequence(contacts).map(HashSet::ofAll);


        var result = activeCustomer = NationalId.of(10000000001L)
                .combine(Name.of("Jonas"))
                .combine(MiddleName.of(null))
                .combine(FamilyName.of("Jonaitis"))
                .combine(contactsSet)
                .ap(ActiveCustomer::new)
                .mapError(Flattener::flatten);

        assertThat(result.isValid()).isTrue();
        assertThat(result.getOrNull()).isExactlyInstanceOf(ActiveCustomer.class);


    }


}