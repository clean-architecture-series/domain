package domain.customer;

import domain.customer.vo.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;
import validation.Flattener;

import static org.assertj.core.api.Assertions.assertThat;

class CreateActiveCustomerTest {

    @Test
    void testSuccess() {


        var contacts = HashSet.of(Mobile.of(800000001), Email.of("jonas@jonaitis.com"));
        Validation<Seq<String>, Set<Contact>> contactsSet =
                Validation.sequence(contacts).map(HashSet::ofAll);


        var result = NationalId.of(10000000001L)
                .combine(Name.of("Jonas"))
                .combine(MiddleName.of("Mykolas"))
                .combine(FamilyName.of("Jonaitis"))
                .combine(contactsSet)
                .ap(ActiveCustomer::new);

        assertThat(result.isValid()).isTrue();
        assertThat(result.getOrNull()).isExactlyInstanceOf(ActiveCustomer.class);

    }

    @Test
    void testFail() {


        var contacts = HashSet.of(Mobile.of(null), Email.of(null));
        Validation<Seq<String>, Set<Contact>> contactsSet =
                Validation.sequence(contacts).map(HashSet::ofAll);


        var result = NationalId.of(null)
                .combine(Name.of(null))
                .combine(MiddleName.of(null))
                .combine(FamilyName.of(null))
                .combine(contactsSet)
                .ap(ActiveCustomer::new)
                .mapError(Flattener::flatten);


        assertThat(result.isValid()).isFalse();
        assertThat(result.getError()).isInstanceOf(List.class);
        assertThat(result.getError()).hasSize(13);


    }


}