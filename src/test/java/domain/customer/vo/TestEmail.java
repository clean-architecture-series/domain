package domain.customer.vo;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestEmail {

    @ParameterizedTest
    @NullAndEmptySource
    void testUndefined(String input) {

        var validation = Email.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactlyInAnyOrder(
                "Email: is mandatory",
                "Email: is not a valid address");

    }


    @ParameterizedTest
    @ValueSource(strings = {"abc", "123"})
    void testInvalid(String input) {

        var validation = Email.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Email: is not a valid address");


    }


    @ParameterizedTest
    @ValueSource(strings = {"Jonas@jonaitis.com"})
    void testValidValue(String input) {

        var validation = Email.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(Email::value).getOrNull()).isEqualTo(input);

    }

}
