package domain.customer.vo;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestFirstName {

    @ParameterizedTest
    @NullAndEmptySource
    void testUndefined(String input) {

        var validation = Name.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactlyInAnyOrder(
                "Name: is mandatory",
                "Name: length must be 1 to 20",
                "Name: must contain letters only");

    }


    @ParameterizedTest
    @ValueSource(strings = {" ", "1"})
    void testLettersOnly(String input) {

        var validation = Name.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Name: must contain letters only");


    }

    @ParameterizedTest
    @ValueSource(ints = {21})
    void testLength(Integer count) {

        var validation = Name.of("A".repeat(count));
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Name: length must be 1 to 20");


    }


    @ParameterizedTest
    @ValueSource(strings = {"Jonas"})
    void testValidValue(String input) {

        var validation = Name.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(Name::value).getOrNull()).isEqualTo(input);

    }

}
