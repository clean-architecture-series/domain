package domain.customer.vo;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestMobile {

    @ParameterizedTest
    @NullSource
    void testUndefined(Integer input) {

        var validation = Mobile.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactlyInAnyOrder(
                "Mobile: is mandatory",
                "Mobile: length must be exactly 9");

    }


    @ParameterizedTest
    @ValueSource(ints = {0, 123})
    void testInvalid(Integer input) {

        var validation = Mobile.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Mobile: length must be exactly 9");

    }


    @ParameterizedTest
    @ValueSource(ints = {860000001})
    void testValidValue(Integer input) {

        var validation = Mobile.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(Mobile::value).getOrNull()).isEqualTo(input);

    }

}
