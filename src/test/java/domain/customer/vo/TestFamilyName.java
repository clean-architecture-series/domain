package domain.customer.vo;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestFamilyName {

    @ParameterizedTest
    @NullAndEmptySource
    void testUndefined(String input) {

        var validation = FamilyName.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactlyInAnyOrder(
                "Family name: is mandatory",
                "Family name: length must be 1 to 30",
                "Family name: must contain letters only");

    }


    @ParameterizedTest
    @ValueSource(strings = {" ", "1"})
    void testLettersOnly(String input) {

        var validation = FamilyName.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Family name: must contain letters only");


    }

    @ParameterizedTest
    @ValueSource(ints = {31})
    void testLength(Integer count) {

        var validation = FamilyName.of("A".repeat(count));
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Family name: length must be 1 to 30");


    }


    @ParameterizedTest
    @ValueSource(strings = {"Jonaitis"})
    void testValidValue(String input) {

        var validation = FamilyName.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(FamilyName::value).getOrNull()).isEqualTo(input);

    }

}
