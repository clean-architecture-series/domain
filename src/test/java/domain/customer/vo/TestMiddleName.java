package domain.customer.vo;


import io.vavr.control.Option;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestMiddleName {

    @ParameterizedTest
    @NullAndEmptySource
    void testUndefined(String input) {

        var validation = MiddleName.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(Option.none());
    }


    @ParameterizedTest
    @ValueSource(strings = {" ", "1"})
    void testLettersOnly(String input) {

        var validation = MiddleName.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Middle name: must contain letters only");


    }

    @ParameterizedTest
    @ValueSource(ints = {31})
    void testLength(Integer count) {

        var validation = MiddleName.of("A".repeat(count));
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Middle name: length must be 1 to 20");


    }


    @ParameterizedTest
    @ValueSource(strings = {"Mykolas"})
    void testValidValue(String input) {

        var validation = MiddleName.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(Option::getOrNull).map(MiddleName::value).getOrNull()).isEqualTo(input);

    }

}
