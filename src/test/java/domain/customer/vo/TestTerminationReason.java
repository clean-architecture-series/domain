package domain.customer.vo;


import io.vavr.control.Option;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestTerminationReason {

    @ParameterizedTest
    @NullAndEmptySource
    void testUndefined(String input) {

        var validation = TerminationReason.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.getOrNull()).isEqualTo(Option.none());
    }


    @ParameterizedTest
    @ValueSource(strings = {"No\n", "No\treason"})
    void testInvalid(String input) {

        var validation = TerminationReason.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly(
                "Termination reason: length must be 10 to 1024",
                "Termination reason: must contain only letters, digits or spaces");


    }

    @ParameterizedTest
    @ValueSource(strings = {"Not using it"})
    void testValidValue(String input) {

        var validation = TerminationReason.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(Option::getOrNull).map(TerminationReason::value).getOrNull()).isEqualTo(input);

    }

}
