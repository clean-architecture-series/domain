package domain.customer.vo;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class TestNationalId {

    @ParameterizedTest
    @NullSource
    void testUndefined(Long input) {

        var validation = NationalId.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactlyInAnyOrder(
                "Id: is mandatory",
                "Id: must start with one of: 1,2,3,4,5,6.",
                "Id: length must be exactly 11");

    }

    @ParameterizedTest
    @ValueSource(longs = {0L, 7L, 8L, 9L})
    void testStartWith(Long input) {

        var validation = NationalId.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactlyInAnyOrder(
                "Id: must start with one of: 1,2,3,4,5,6.",
                "Id: length must be exactly 11");

    }

    @ParameterizedTest
    @ValueSource(longs = {123456789L})
    void testFailLength(Long input) {

        var validation = NationalId.of(input);
        assertThat(validation.isValid()).isFalse();
        assertThat(validation.getError()).containsExactly("Id: length must be exactly 11");

    }


    @ParameterizedTest
    @ValueSource(longs = {10000000001L, 20000000001L})
    void testValidValue(Long input) {

        var validation = NationalId.of(input);
        assertThat(validation.isValid()).isTrue();
        assertThat(validation.map(NationalId::value).getOrNull()).isEqualTo(input);

    }

}
