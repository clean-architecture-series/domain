package validation;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FlattenerTest {


    @Test
    void testSuccess() {
        Seq<Seq<String>> nested = List.of(List.of("A", "B"), List.of("A", "Z"));
        var flat = Flattener.flatten(nested);
        Seq<String> expected = List.of("A", "B", "A", "Z");
        assertThat(flat).isEqualTo(expected);
    }

}