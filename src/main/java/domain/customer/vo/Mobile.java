package domain.customer.vo;

import domain.AValueObject;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import validation.MustBeNotNull;
import validation.MustHaveExactLength;
import validation.SequentialValidation;

// #region 1
public final class Mobile extends AValueObject<Integer>
        implements Contact {

    public static final String CAPTION = "Mobile";

    private static final MustHaveExactLength<Integer> mustHaveNineDigits = MustHaveExactLength.of(9);

    private static final SequentialValidation<Integer> validation = SequentialValidation.<Integer>of(CAPTION)
            .validate(MustBeNotNull::validate)
            .validate(mustHaveNineDigits::validate);

    private Mobile(Integer value) {
        super(value);
    }

    public static Validation<Seq<String>, Mobile> of(Integer value) {

        return validation.apply(value)
                .map(Mobile::new);
    }
}
// #endregion 1


