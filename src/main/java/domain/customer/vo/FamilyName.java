package domain.customer.vo;


import domain.AValueObject;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import validation.MustBeAlpha;
import validation.MustBeNotNull;
import validation.MustHaveLenghtInRange;
import validation.SequentialValidation;

// #region 1
public final class FamilyName extends AValueObject<String> {

    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 30;

    private static final String CAPTION = "Family name";

    private static final MustHaveLenghtInRange<String> stringLength =
            MustHaveLenghtInRange.of(MIN_LENGTH, MAX_LENGTH);

    private static final SequentialValidation<String> validation = SequentialValidation.<String>of(CAPTION)
            .validate(MustBeNotNull::validate)
            .validate(stringLength::validate)
            .validate(MustBeAlpha::validate);

    // #region construct

    private FamilyName(String value) {
        super(value);
    }

    public static Validation<Seq<String>, FamilyName> of(String value) {

        return validation.apply(value)
                .map(FamilyName::new);

    }
}
// #endregion 1
