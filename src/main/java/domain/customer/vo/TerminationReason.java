package domain.customer.vo;

import domain.AValueObject;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import validation.Maybe;
import validation.MustBeAsciiPrintable;
import validation.MustHaveLenghtInRange;
import validation.SequentialValidation;

// #region 1
public final class TerminationReason extends AValueObject<String> {

    private static final int MIN_LENGTH = 10;
    private static final int MAX_LENGTH = 1024;
    private static final String CAPTION = "Termination reason";

    private static final MustHaveLenghtInRange<String> stringLength =
            MustHaveLenghtInRange.of(MIN_LENGTH, MAX_LENGTH);

    private static final SequentialValidation<String> validation = SequentialValidation.<String>of(CAPTION)
            .validate(stringLength::validate)
            .validate(MustBeAsciiPrintable::validate);

    private TerminationReason(String value) {
        super(value);
    }

    public static Validation<Seq<String>, Option<TerminationReason>> of(String value) {

        return Maybe.defined(value)
                .map(TerminationReason::validate)
                .getOrElse(Validation.valid(Option.none()));

    }

    private static Validation<Seq<String>, Option<TerminationReason>> validate(String value) {

        return validation.apply(value)
                .map(TerminationReason::new)
                .map(Option::of);

    }
}
// #endregion 1
