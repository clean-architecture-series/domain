package domain.customer.vo;

import domain.AValueObject;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import validation.MustBeEmail;
import validation.MustBeNotNull;
import validation.SequentialValidation;

// #region 1
public final class Email extends AValueObject<String>
        implements Contact {

    public static final String CAPTION = "Email";

    private static final SequentialValidation<String> validation =
            SequentialValidation.<String>of(CAPTION)
                    .validate(MustBeNotNull::validate)
                    .validate(MustBeEmail::validate);

    private Email(String value) {
        super(value);
    }

    public static Validation<Seq<String>, Email> of(String value) {

        return validation.apply(value)
                .map(Email::new);
    }
}
// #endregion 1