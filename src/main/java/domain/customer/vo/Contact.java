package domain.customer.vo;

// #region type
public sealed interface Contact permits Email, Mobile {

}
// #endregion type