package domain.customer.vo;

import domain.AValueObject;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import validation.Maybe;
import validation.MustBeAlpha;
import validation.MustHaveLenghtInRange;
import validation.SequentialValidation;

// #region 1
public final class MiddleName extends AValueObject<String> {

    private static final String CAPTION = "Middle name";
    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 20;


    private static final MustHaveLenghtInRange<String> stringLength =
            MustHaveLenghtInRange.of(MIN_LENGTH, MAX_LENGTH);

    private static final SequentialValidation<String> validation = SequentialValidation.<String>of(CAPTION)
            .validate(stringLength::validate)
            .validate(MustBeAlpha::validate);

    private MiddleName(String value) {
        super(value);
    }

    public static Validation<Seq<String>, Option<MiddleName>> of(String value) {

        return Maybe.defined(value)
                .map(MiddleName::validate)
                .getOrElse(Validation.valid(Option.none()));

    }

    private static Validation<Seq<String>, Option<MiddleName>> validate(String value) {
        return validation.apply(value)
                .map(MiddleName::new)
                .map(Option::of);
    }
}
// #endregion 1
