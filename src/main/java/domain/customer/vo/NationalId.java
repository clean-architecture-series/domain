package domain.customer.vo;


import domain.AValueObject;
import io.vavr.collection.LinkedHashSet;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import validation.MustBeNotNull;
import validation.MustHaveExactLength;
import validation.MustStartWithOneOf;
import validation.SequentialValidation;

// #region 1
public final class NationalId extends AValueObject<Long> {

    public static final String CAPTION = "Id";
    private static final int EXACT_LENGTH = 11;

    private static final MustStartWithOneOf<Long> mustStartWithOneOf =
            new MustStartWithOneOf<>(
                    LinkedHashSet.of(1L, 2L, 3L, 4L, 5L, 6L)
            );

    private static final MustHaveExactLength<Long> mustHaveElevenDigits =
            MustHaveExactLength.of(EXACT_LENGTH);

    private static final SequentialValidation<Long> validation =
            SequentialValidation.<Long>of(CAPTION)
                    .validate(MustBeNotNull::validate)
                    .validate(mustStartWithOneOf::validate)
                    .validate(mustHaveElevenDigits::validate);


    private NationalId(Long value) {
        super(value);
    }

    public static Validation<Seq<String>, NationalId> of(Long value) {

        return validation.apply(value)
                .map(NationalId::new);
    }
}
// #endregion 1
