package domain.customer;


import domain.customer.vo.*;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import lombok.Getter;
import lombok.ToString;


@Getter
@ToString(callSuper = true)
// #region 1
public final class ActiveCustomer extends Customer<NationalId> {

    private final Name name;
    // #region 4
    private final Option<MiddleName> middleName;
    // #endregion 4
    private final FamilyName familyName;

    private final Set<Contact> contacts;

    public ActiveCustomer(NationalId id,
                          Name name,
                          Option<MiddleName> middleName,
                          FamilyName familyName,
                          Set<Contact> contacts) {
        super(id);
        this.name = name;
        this.middleName = middleName;
        this.familyName = familyName;
        this.contacts = contacts;
    }
    // #endregion 1
    // #region 2

    public ActiveCustomer update(Name name,
                                 Option<MiddleName> middleName,
                                 FamilyName familyName,
                                 Set<Contact> contacts) {

        return new ActiveCustomer(getId(),
                name, middleName, familyName, contacts);

    }
    // #endregion 2

    // #region 3

    public UnsubscribedCustomer unsubscribe(Option<TerminationReason> reason) {

        return new UnsubscribedCustomer(getId(), reason);
    }

    // #endregion 3


}

