package domain.customer;


import domain.customer.vo.NationalId;
import domain.customer.vo.TerminationReason;
import io.vavr.control.Option;
import lombok.Getter;
import lombok.ToString;


@Getter
@ToString(callSuper = true)
// #region 1
public final class
UnsubscribedCustomer extends Customer<NationalId> {

    private final Option<TerminationReason> termination;

    public UnsubscribedCustomer(NationalId id,
                                Option<TerminationReason> termination) {
        super(id);
        this.termination = termination;
    }

// #endregion 1


}

