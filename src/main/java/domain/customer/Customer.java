package domain.customer;


import lombok.ToString;

import java.util.Objects;

// #region 1
@ToString
abstract sealed class Customer<T>
        permits ActiveCustomer, UnsubscribedCustomer {

    private final T id;

    Customer(T id) {
        this.id = id;
    }

    @Override
    public final boolean equals(Object o) {
        return o instanceof Customer<?> entity
                && Objects.equals(this.id, entity.id);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(id);
    }
    // #endregion 1

    public T getId() {
        return id;
    }
}

