package domain;

import java.util.Objects;

public abstract class AValueObject<T> {

    private final T value;

    protected AValueObject(T value) {
        this.value = value;
    }

    public T value() {
        return value;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AValueObject<?> that = (AValueObject<?>) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public final int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public final String toString() {
        return String.format("%s(%s)", this.getClass().getSimpleName(), value);
    }
}
