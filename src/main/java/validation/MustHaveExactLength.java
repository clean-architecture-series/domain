package validation;

public final class MustHaveExactLength<T> extends AbstractLengthValidation<T> {

    private static final String MESSAGE_FORMAT = "length must be exactly %d";


    private MustHaveExactLength(int exactLenght) {
        super(MESSAGE_FORMAT, exactLenght, exactLenght);
    }

    public static <S> MustHaveExactLength<S> of(int exactLenght) {
        return new MustHaveExactLength<>(exactLenght);
    }


}
