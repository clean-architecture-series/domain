package validation;

import io.vavr.collection.Set;
import io.vavr.control.Option;
import io.vavr.control.Validation;


public final class MustStartWithOneOf<T> {

    private static final String MESSAGE_PREFIX = "must start with one of: ";
    private static final String MESSAGE_DELIMITER = ",";
    private static final String MESSAGE_SEFFIX = ".";

    private final Set<T> values;

    private final String message;

    public MustStartWithOneOf(Set<T> values) {
        this.values = values;
        this.message = values.mkString(MESSAGE_PREFIX, MESSAGE_DELIMITER, MESSAGE_SEFFIX);
    }

    public Validation<String, T> validate(T value) {
        return Option.of(value)
                .map(this::validatePrefix)
                .getOrElse(Validation.invalid(message));

    }


    private Validation<String, T> validatePrefix(T value) {

        String valueString = String.valueOf(value);
        return values.map(String::valueOf)
                .exists(valueString::startsWith) ?
                Validation.valid(value) : Validation.invalid(message);

    }


}
