package validation;

import io.vavr.control.Validation;
import lombok.experimental.UtilityClass;
import org.apache.commons.validator.routines.EmailValidator;

@UtilityClass
public final class MustBeEmail {

    private static final String MESSAGE = "is not a valid address";

    public static Validation<String, String> validate(String value) {
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(value) ? Validation.valid(value) : Validation.invalid(MESSAGE);

    }

}
