package validation;

import io.vavr.control.Option;
import io.vavr.control.Validation;


public abstract class AbstractLengthValidation<T> {


    private final int minLength;
    private final int maxLength;
    private final String message;

    AbstractLengthValidation(String messageFormat, int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.message = String.format(messageFormat, minLength, maxLength);
    }


    public final Validation<String, T> validate(T value) {
        return Option.of(value)
                .map(this::validateValue)
                .getOrElse(Validation.invalid(message));

    }


    private Validation<String, T> validateValue(T value) {

        return valueOutOfRange(value) ?
                Validation.invalid(message) : Validation.valid(value);
    }

    private boolean valueOutOfRange(T value) {

        String valueString = String.valueOf(value);
        return valueString.length() < minLength || valueString.length() > maxLength;
    }

}
