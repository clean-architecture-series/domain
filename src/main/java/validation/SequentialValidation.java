package validation;

import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

import java.util.function.Function;

public final class SequentialValidation<A> {

    private static final String MESSAGE_FORMAT = "%s: %s";

    private final String messagePrefix;

    private final List<Function<A, Validation<String, A>>> validations;

    private SequentialValidation(String messagePrefix, List<Function<A, Validation<String, A>>> validations) {
        this.messagePrefix = messagePrefix;
        this.validations = validations;
    }

    public static <A> SequentialValidation<A> of(String messagePrefix) {
        return new SequentialValidation<>(messagePrefix, List.empty());
    }

    public SequentialValidation<A> validate(Function<A, Validation<String, A>> validation) {

        return new SequentialValidation<>(messagePrefix, validations.append(validation));
    }

    public Validation<Seq<String>, A> apply(A value) {

        var listOfResults = validations.map(validation -> validation.apply(value))
                .map(this::addPrefix);

        var combinedResult = combinedResult(listOfResults);

        return combinedResult.map(success -> success.getOrElse(value));

    }

    private Validation<Seq<String>, A> addPrefix(Validation<String, A> validation) {

        return validation.mapError(this::format)
                .mapError(List::of);
    }

    private String format(String error) {

        return String.format(MESSAGE_FORMAT, messagePrefix, error);

    }

    private Validation<Seq<String>, Seq<A>> combinedResult(Iterable<Validation<Seq<String>, A>> validations) {

        return Validation.sequence(validations)
                .mapError(List::ofAll);

    }
}
