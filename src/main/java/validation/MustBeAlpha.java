package validation;

import io.vavr.control.Validation;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

@UtilityClass
public final class MustBeAlpha {

    private static final String MESSAGE = "must contain letters only";


    public static Validation<String, String> validate(String value) {

        return StringUtils.isAlpha(value) ? Validation.valid(value) :
                Validation.invalid(MESSAGE);

    }
}
