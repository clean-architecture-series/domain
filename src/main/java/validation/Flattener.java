package validation;

import io.vavr.collection.Seq;
import lombok.experimental.UtilityClass;

import java.util.function.Function;

@UtilityClass
public class Flattener {


    public static Seq<String> flatten(Seq<Seq<String>> nested) {
        return nested.flatMap(Function.identity());
    }

}
