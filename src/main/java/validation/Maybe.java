package validation;

import io.vavr.control.Option;
import lombok.experimental.UtilityClass;

import java.util.function.Predicate;

@UtilityClass
public class Maybe {


    public static Option<String> defined(String value) {
        return Option.of(value)
                .filter(Predicate.not(String::isEmpty));
    }


}
