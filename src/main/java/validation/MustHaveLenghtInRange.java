package validation;

public final class MustHaveLenghtInRange<T> extends AbstractLengthValidation<T> {

    private static final String MESSAGE_FORMAT = "length must be %d to %d";


    private MustHaveLenghtInRange(int minLength, int maxLength) {
        super(MESSAGE_FORMAT, minLength, maxLength);
    }

    public static <S> MustHaveLenghtInRange<S> of(int minLength, int maxLength) {
        return new MustHaveLenghtInRange<>(minLength, maxLength);
    }


}
