package validation;

import io.vavr.control.Option;
import io.vavr.control.Validation;
import lombok.experimental.UtilityClass;

import java.util.function.Predicate;

@UtilityClass
public final class MustBeNotNull {

    private static final String MESSAGE = "is mandatory";

    public static <T> Validation<String, T> validate(T value) {
        return Option.of(value)
                .map(String::valueOf)
                .filter(Predicate.not(String::isEmpty))
                .map(validString -> value)
                .toValidation(MESSAGE);

    }

}
