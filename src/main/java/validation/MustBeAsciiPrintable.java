package validation;

import io.vavr.control.Validation;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

@UtilityClass
public final class MustBeAsciiPrintable {

    private static final String MESSAGE = "must contain only letters, digits or spaces";


    public static Validation<String, String> validate(String value) {

        return StringUtils.isAsciiPrintable(value) ? Validation.valid(value) :
                Validation.invalid(MESSAGE);

    }


}
